"use strict";

var React = require('react');

var Settings = React.createClass({
	statics: {
		willTransitionTo: function(transition, params, query, callback) {
			if(!isLoggedIn) {
				transition.abort();
				callback();
			}
		},

		willTransitionFrom: function(transition, component) {
			if(component.formHasUnsavedDate()) {
				if(!confirm('Sure you want to leave without saving?')) {
					transition.abort();
				}
			}
		}
	}
});

module.exports = Settings;