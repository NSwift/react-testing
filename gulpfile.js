"use strict";

var gulp = require('gulp');
var connect = require('gulp-connect');			// runs a local dev server
var open = require('gulp-open');				// open a url in web browser
var browserify = require('browserify');			// bundles JS
var reactify = require('reactify');				// transforms React JSX to JS
var source = require('vinyl-source-stream');	// use conventional text streams with Gulp
var concat = require('gulp-concat');			// concatenates files
var lint = require('gulp-eslint');				// lint JS files, including JSX files
var minifyCss = require('gulp-minify-css');		// minify CSS
var minifyJs = require('gulp-uglify');			// minify JS
var rename = require("gulp-rename");			// rename files

var config = {
	port: 9005,
	devBaseUrl: 'http://localhost',
	paths: {
		html: './src/*.html',
		js: './src/**/*.js',
		images: './src/images/*',
		css: [
			'node_modules/bootstrap/dist/css/bootstrap.min.css',
			'node_modules/bootstrap/dist/css/bootstrap-theme.min.css',
			'node_modules/toastr/toastr.css'			
		],
		dist: './dist',
		mainJs: './src/main.js'
	}
};

// start a local development server
gulp.task('connect', function(){
	connect.server({
		root: ['dist'],
		port: config.port,
		base: config.devBaseUrl,
		livereload: true
	});
});

gulp.task('open', ['connect'], function() {
	gulp.src('dist/index.html')
		.pipe(open({ uri: config.devBaseUrl + ':' + config.port + '/' }));
});

// move html files from src folder to dist folder
gulp.task('html', function() {
	gulp.src(config.paths.html)
		.pipe(gulp.dest(config.paths.dist))
		.pipe(connect.reload());
});

// transforms jsx into js, bundles js, moves to dist folder and reloads browser
gulp.task('js', function() {
	browserify(config.paths.mainJs)
		.transform(reactify)
		.bundle()
		.on('error', console.error.bind(console))
		.pipe(source('bundle.js'))
		.pipe(gulp.dest(config.paths.dist + '/scripts'))
		.pipe(connect.reload());
});

gulp.task('compress', ['js'], function() {
  return gulp.src(config.paths.dist + '/scripts/*.js')
    .pipe(minifyJs())
    .pipe(rename({
		extname: '.min.js'
	}))
    .pipe(gulp.dest(config.paths.dist + '/scripts/'));
});

// bundle css from configured css paths and bundle into bundle.css
gulp.task('css', function() {
	gulp.src(config.paths.css)
		.pipe(minifyCss())
		.pipe(concat('bundle.css'))
		.pipe(rename({
			extname: '.min.css'
		}))
		.pipe(gulp.dest(config.paths.dist + '/css'));
});

// migrates images to dist folder
// note: could even optimise images here
gulp.task('images', function() {
	gulp.src(config.paths.images)
		.pipe(gulp.dest(config.paths.dist + '/images'))
		.pipe(connect.reload());

	// publish favicon
	gulp.src('./src/favicon.ico')
		.pipe(gulp.dest(config.paths.dist));
});

// adds linting for JS, uses rules from eslint.config.json
gulp.task('lint', function() {
	return gulp.src(config.paths.js)
		.pipe(lint({config: 'eslint.config.json'}))
		.pipe(lint.format());
});

// anytime anything changes in html path, run html task
// each time JS changes also lint
gulp.task('watch', function() {
	gulp.watch(config.paths.html, ['html']);
	gulp.watch(config.paths.js, ['js', 'lint']);	
});

// default tasks to run when 'gulp' command is used on the command line
gulp.task('default', ['html', 'js', 'css', 'images', 'lint', 'open', 'watch']);