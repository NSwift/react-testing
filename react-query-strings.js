// given a route like this:
<route path="/course/:courseId" handler={course} />

// and a URL like this:
'/course/clean-code?modules=3'

// the component's props will be populated like:
var Course = React.createClass({
	render: function() {
		this.props.params.courseId; // "clean-code"
		this.props.query.module; 	// 3
		this.props.path; 			// "/course/clean-code/?module=3"
	}
});