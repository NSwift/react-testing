// dispatcher is a singleton
// one dispatcher per application
var Dispatcher = require('flux').Dispatcher;

module.exports = new Dispatcher();