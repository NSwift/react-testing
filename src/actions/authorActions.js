"use strict";

var Dispatcher = require('../dispatcher/appDispatcher');
var AuthorApi = require('../api/authorApi');
var ActionTypes = require('../constants/actionTypes');

var AuthorActions = {

	// action creator
	createAuthor: function(author) {
		var newAuthor = AuthorApi.saveAuthor(author);

		// action
		// hey dispatcher, go tell all the stores that an author was just created
		Dispatcher.dispatch({
			actionType: ActionTypes.CREATE_AUTHOR,
			author: newAuthor
		});
	},

	// update author
	updateAuthor: function(author) {
		var updatedAuthor = AuthorApi.saveAuthor(author);

		Dispatcher.dispatch({
			actionType: ActionTypes.UPDATE_AUTHOR,
			author: updatedAuthor
		});
	},

	deleteAuthor: function(id) {
		// debugger;
		AuthorApi.deleteAuthor(id);

		Dispatcher.dispatch({
			actionType: ActionTypes.DELETE_AUTHOR,
			id: id
		});
	},

	updateViewedAuthor: function(id) {
		debugger;
		// get some data from the api here
		var currentlyViewedAuthor = AuthorApi.getAuthorById(id);

		Dispatcher.dispatch({
			actionType: ActionTypes.UPDATE_VIEWED_AUTHOR,
			author: currentlyViewedAuthor
		});
	}

};

module.exports = AuthorActions;