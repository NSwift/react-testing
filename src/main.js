/*
	Entry point for the application
*/

"use strict";

var React = require('react');
var Router = require('react-router');
var Routes = require('./routes');
var InitialiseActions = require('./actions/initialiseActions');

// fires the init app action that will send the initial data
// to all stores and bootstrap the application
InitialiseActions.initApp();

// history location does not work on IE8
// routing url style has changed in React Router 1.0
//Router.run(Routes, Router.HistoryLocation, function(Handler) {
Router.run(Routes, function(Handler) {
	React.render(<Handler/>, document.getElementById('app'));
});