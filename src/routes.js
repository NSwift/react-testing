"use strict";

var React = require('react');
var Router = require('react-router');
var DefaultRoute = Router.DefaultRoute;
var Route = Router.Route;
var NotFoundRoute = Router.NotFoundRoute;
var Redirect = Router.Redirect;

// without defining a path, React Router will automatically assume
// the specified name is the route, i.e. /authors
// to use different route:
// <Route name="about" path="about-us" handler={require('./components/about/aboutPage')} />

var routes = (
	<Route name="app" path="/" handler={require('./components/app')}>
		<DefaultRoute handler={require('./components/homePage')} />
		<Route name="authors" handler={require('./components/authors/authorPage')} />
		<Route name="addAuthor" path="author" handler={require('./components/authors/manageAuthorPage')} />
		<Route name="manageAuthor" path="author/:id" handler={require('./components/authors/manageAuthorPage')} />
		<Route name="about" handler={require('./components/about/aboutPage')} />
		<NotFoundRoute handler={require('./components/notFoundPage')} />
		<Redirect from="about-us" to="about" />
		<Redirect from="about/*" to="about" /> // perhaps the about page used to have sub-directories
	</Route>
);

module.exports = routes;