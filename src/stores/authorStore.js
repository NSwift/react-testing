"use strict";

var Dispatcher = require('../dispatcher/appDispatcher');
var ActionTypes = require('../constants/ActionTypes');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var _ = require('lodash');
var CHANGE_EVENT = 'change';

var _authors = [];
var _viewedAuthor = {
						id: 'default-author', 
						firstName: 'Default', 
						lastName: 'Author',
						description: 'XXX 000'
					};	

// use object assign to take an empty object, extend it with the
// Node.js EventEmitter objects prototype and then add further functionality
var AuthorStore = assign({}, EventEmitter.prototype, {
	addChangeListener: function(callback) {
		this.on(CHANGE_EVENT, callback);
	},

	removeChangeListener: function(callback) {
		this.removeListener(CHANGE_EVENT, callback);
	},

	emitChange: function() {
		this.emit(CHANGE_EVENT);
	},

	getAllAuthors: function() {
		return _authors;
	},

	getAuthorById: function(id) {
		return _.find(_authors, {id: id});
	},

	getViewedAuthor: function() {
		return _viewedAuthor;
	}
});

Dispatcher.register(function(action) {
	switch(action.actionType) {

		// populate the authors array with
		// data grabbed from the initial data bootstrapping action
		case ActionTypes.INITIALISE:
			_authors = action.initialData.authors;
			AuthorStore.emitChange();
			break;

		case ActionTypes.CREATE_AUTHOR:
			_authors.push(action.author);

			// call emitChange whenever a store changes
			// by emitting a change, any components that have registered with
			// the store will be notified and know to update the ui accordingly
			AuthorStore.emitChange();
			break;

		case ActionTypes.UPDATE_AUTHOR:
			var existingAuthor = _.find(_authors, {id: action.author.id});
			var existingAuthorIndex = _.indexOf(_authors, existingAuthor);
			_authors.splice(existingAuthorIndex, 1, action.author);
			AuthorStore.emitChange();
			break;

		case ActionTypes.DELETE_AUTHOR:
			// debugger;
			_.remove(_authors, function(author) {
				return action.id === author.id;
			});
			AuthorStore.emitChange();
			break;

		case ActionTypes.UPDATE_VIEWED_AUTHOR:
			debugger;
			_viewedAuthor = action.author;
			AuthorStore.emitChange();
			break;

		default:
			// no op
	}
});

module.exports = AuthorStore;
