"use strict";

var keyMirror = require('react/lib/keyMirror');

// keyMirror copies key to value inside of module.exports
module.exports = keyMirror({
	INITIALISE: null,
	CREATE_AUTHOR: null,
	UPDATE_AUTHOR: null,
	DELETE_AUTHOR: null,
	UPDATE_VIEWED_AUTHOR: null
});