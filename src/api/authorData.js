module.exports = {
	authors: 
	[
		{
			id: 'cory-house', 
			firstName: 'Cory', 
			lastName: 'House',
			description: 'ABC 123'
		},	
		{
			id: 'scott-allen', 
			firstName: 'Scott', 
			lastName: 'Allen',
			description: 'DEF 456'
		},	
		{
			id: 'dan-wahlin', 
			firstName: 'Dan', 
			lastName: 'Wahlin',
			description: 'GHI 789'
		}
	]
};