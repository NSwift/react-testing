/*eslint-disable strict */ // disable check because we can't run strict mode, need global variables, jQuery.

var React = require('react');
var Header = require('./common/header');
var RouteHandler = require('react-router').RouteHandler;
$ = jQuery = require('jquery');

// look at route and set Child property on app react class
// then render the Child property as a child component
var App = React.createClass({
	render: function() {
		return (
			<div>
				<Header/>
				<div className="container-fluid">
					<RouteHandler/>
				</div>
			</div>
		);
	}
});

module.exports = App;