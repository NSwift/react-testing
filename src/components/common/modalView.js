"use strict";

var React = require('react');

var ModalAuthorView = React.createClass({
	render: function() {

		var classString = "hidden";
		if(this.props.author.firstName !== "Default") {
			classString = "dodgy-div";
		}
		
		return (
			<div className={classString}>
				<h2>{this.props.author.firstName} {this.props.author.lastName}</h2>
				<p>{this.props.author.description}</p>
			</div>
		);
	}
});

module.exports = ModalAuthorView;