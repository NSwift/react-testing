"use strict";

var React = require('react');
var Router = require('react-router');
var Link = Router.Link;
var AuthorActions = require('../../actions/AuthorActions');
var AuthorStore = require('../../stores/authorStore');
var AuthorList = require('./authorList');
var ModalAuthorView = require('../common/modalView');

var AuthorPage = React.createClass({
	getInitialState: function() {
		return {
			// can rely on AuthorStore to provide an empty array
			// or whatever authors are currently stored in the AuthorStore
			authors: AuthorStore.getAllAuthors(),
			viewedAuthor: AuthorStore.getViewedAuthor()
		};
	},

	componentWillMount: function() {
		AuthorStore.addChangeListener(this._onChange);
	},

	// clean up when component is unmounted
	componentWillUnmount: function() {
		AuthorStore.removeChangeListener(this._onChange);
	},

	_onChange: function() {
		debugger;
		this.setState({ authors: AuthorStore.getAllAuthors() });
		this.setState({ viewedAuthor: AuthorStore.getViewedAuthor() });
	},

	render: function() {

		var createAuthorRow = function(author) {
			return (
				// when creating multiple instances of a given component, react
				// requires a key so that it can keep track of them.
				// uses to maintain proper state and order of elements.
				// typically db primary key used.
				<tr key={author.id}>
					<td><a href={"/#authors/" + author.id}>{author.id}</a></td>
					<td>{author.firstName} {author.lastName}</td>
				</tr>
			);
		};

		return (
			<div>
				<h1>Authors</h1>
				<Link to="addAuthor" className="btn btn-default">Add Author</Link>
				<AuthorList authors={this.state.authors} />
				<ModalAuthorView author={this.state.viewedAuthor} />
			</div>
		);
	}
});

module.exports = AuthorPage;