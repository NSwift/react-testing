"use strict";

var React = require('react');
var Router = require('react-router');
var Link = Router.Link;
var AuthorActions = require('../../actions/authorActions');
var Toastr = require('toastr');

var AuthorList = React.createClass({

	// enforce required props required by the component at dev time.
	// if any prop is not supplied error is logged in browser console.
	propTypes: {
		authors: React.PropTypes.array.isRequired
	},

	deleteAuthor: function(id, event) {
		event.preventDefault();
		// debugger;
		AuthorActions.deleteAuthor(id);
		Toastr.success('Author Deleted');
	},

	viewAuthor: function(id, event) {
		event.preventDefault();
		debugger;
		AuthorActions.updateViewedAuthor(id);
	},

	render: function() {

		var createAuthorRow = function(author) {
			return (
				<tr key={author.id}>
					<td><a href="#" onClick={this.deleteAuthor.bind(this, author.id)}>Delete</a></td>
					<td><a href="#" onClick={this.viewAuthor.bind(this, author.id)}>View</a></td>
					<td><Link to="manageAuthor" params={{id: author.id}}>{author.id}</Link></td>
					<td>{author.firstName} {author.lastName}</td>
				</tr>
			);
		};

		return (
			<div>
				<table className="table">
					<thead>
						<th></th>
						<th></th>
						<th>ID</th>
						<th>Name</th>
					</thead>
					<tbody>
						{this.props.authors.map(createAuthorRow, this)}
					</tbody>
				</table>
			</div>
		);
	}
});

module.exports = AuthorList;